package oleksandr.fedoruk.grade.service;

import oleksandr.fedoruk.grade.models.Answer;
import oleksandr.fedoruk.grade.models.Quiz;
import oleksandr.fedoruk.grade.models.QuizToSolve;
import oleksandr.fedoruk.grade.repositories.AnswerRepository;
import oleksandr.fedoruk.grade.repositories.QuestionRepository;
import oleksandr.fedoruk.grade.repositories.QuizRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class QuizService {

    private final QuizRepository repository;
    private final AnswerRepository answerRepository;
    private final QuestionRepository questionRepository;

    @Autowired
    public QuizService(QuizRepository repository, AnswerRepository answerRepository, QuestionRepository questionRepository) {
        this.repository = repository;
        this.answerRepository = answerRepository;
        this.questionRepository = questionRepository;
    }

    public QuizToSolve getQuizToSolve(long id) {
        QuizToSolve quizToSolve = new QuizToSolve();
        Optional<Quiz> quiz = repository.findById(id);

        if (quiz.isPresent()) {
            quizToSolve.setQuiz(quiz.get());
            quizToSolve.setQuestions(questionRepository.findByQuiz(quiz.get()));
        }

        return quizToSolve;
    }

    public List<Quiz> getAll() {
        return repository.findAll();
    }

    public Long save(Quiz n) {
        n = repository.save(n);
        repository.flush();
        return n.getId();
    }

    public Quiz findById(Long parseInt) {
        Optional<Quiz> quiz = repository.findById(parseInt);
        return quiz.orElse(null);
    }

    public int[] solveScore(Map<String, String> allParams) {
        int correctAnswers = 0;
        int countOfAnswers = 0;
        long id;
        for (Map.Entry<String, String> answer : allParams.entrySet()) {
            id = Long.valueOf(answer.getKey());
            Optional<Answer> optionalAnswer = answerRepository.findById(id);

            countOfAnswers++;
            if (optionalAnswer.isPresent()) {
                Answer answerFromDB = optionalAnswer.get();
                if (answerFromDB.getCorrect())
                    correctAnswers++;
            }
        }

        return new int[]{correctAnswers, countOfAnswers};
    }

}