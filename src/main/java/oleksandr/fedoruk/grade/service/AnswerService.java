package oleksandr.fedoruk.grade.service;

import oleksandr.fedoruk.grade.models.Answer;
import oleksandr.fedoruk.grade.models.Question;
import oleksandr.fedoruk.grade.repositories.AnswerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;


@Service
public class AnswerService {

    private final AnswerRepository answerRepository;

    @Autowired
    public AnswerService(AnswerRepository answerRepository) {
        this.answerRepository = answerRepository;
    }


    public List<Answer> getAnswersByQuestionId(Question question) {
        Collection<? extends Answer> answers = answerRepository.findAllByQuestionId(question);
        return (List<Answer>) answers;
    }
}
