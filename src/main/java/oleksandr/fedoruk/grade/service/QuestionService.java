package oleksandr.fedoruk.grade.service;

import oleksandr.fedoruk.grade.models.Answer;
import oleksandr.fedoruk.grade.models.NewQuestion;
import oleksandr.fedoruk.grade.models.Question;
import oleksandr.fedoruk.grade.models.Quiz;
import oleksandr.fedoruk.grade.repositories.AnswerRepository;
import oleksandr.fedoruk.grade.repositories.QuestionRepository;
import oleksandr.fedoruk.grade.repositories.QuizRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class QuestionService {
    private QuestionRepository repository;
    private QuizRepository quizRepository;
    private final AnswerRepository answerRepository;
    private QuestionRepository questionRepository;

    public QuestionService(QuestionRepository repository, AnswerRepository answerRepository, QuestionRepository questionRepository, QuizRepository quizRepository) {
        this.repository = repository;
        this.answerRepository = answerRepository;
        this.questionRepository = questionRepository;
        this.quizRepository = quizRepository;
    }

    public List<Question> findAll() {
        return repository.findAll();
    }


    public Optional<Question> getById(long id) {
        return repository.findById(id);
    }

    public List<Question> findByQuiz(Quiz quiz) {
        return repository.findByQuiz(quiz);
    }

    public void saveQuestion(NewQuestion newQuestion) {
        Optional<Quiz> quiz = quizRepository.findById(newQuestion.quizId);
        Question question = new Question();
        question.setText(newQuestion.text);
        quiz.ifPresent(question::setQuiz);
        questionRepository.save(question);
        boolean isCorrect = false;

        if (newQuestion.correctIs == 1) isCorrect = true;
        answerRepository.save(new Answer(question, newQuestion.answer1, isCorrect));
        if (newQuestion.correctIs == 2) isCorrect = true;
        answerRepository.save(new Answer(question, newQuestion.answer2, isCorrect));
        if (newQuestion.correctIs == 3) isCorrect = true;
        answerRepository.save(new Answer(question, newQuestion.answer3, isCorrect));
        if (newQuestion.correctIs == 4) isCorrect = true;
        answerRepository.save(new Answer(question, newQuestion.answer4, isCorrect));
    }
}
