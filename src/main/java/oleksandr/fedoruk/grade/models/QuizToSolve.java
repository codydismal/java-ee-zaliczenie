package oleksandr.fedoruk.grade.models;

import lombok.Data;

import java.util.List;

@Data
public class QuizToSolve {
    Quiz quiz;
    List<Question> questions;
}
