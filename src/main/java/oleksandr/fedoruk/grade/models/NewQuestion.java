package oleksandr.fedoruk.grade.models;

import lombok.Data;


@Data
public class NewQuestion {
    public Long quizId;
    public String text;
    public String answer1;
    public String answer2;
    public String answer3;
    public String answer4;
    public Integer correctIs;
}
