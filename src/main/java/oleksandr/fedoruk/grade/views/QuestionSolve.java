package oleksandr.fedoruk.grade.views;


import oleksandr.fedoruk.grade.models.Question;
import oleksandr.fedoruk.grade.models.QuizToSolve;
import oleksandr.fedoruk.grade.service.AnswerService;
import oleksandr.fedoruk.grade.service.QuestionService;
import oleksandr.fedoruk.grade.service.QuizService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;


@Controller
public class QuestionSolve {
    QuizService quizService;
    QuestionService questionService;
    AnswerService answerService;

    public QuestionSolve(QuestionService questionService, QuizService quizService, AnswerService answerService) {
        this.questionService = questionService;
        this.quizService = quizService;
        this.answerService = answerService;
    }

    @GetMapping("/solve")
    public String questionSolver(@RequestParam("quizId") long id, Model model) {
        QuizToSolve quizToSolve = quizService.getQuizToSolve(id);
        if (quizToSolve.getQuiz() == null) return "/";

        for (Question question : quizToSolve.getQuestions()) {
            System.out.println(question.getId());
            question.setAnswerList(answerService.getAnswersByQuestionId(question));
        }

        model.addAttribute("quiz", quizToSolve);
        return "quiz";
    }

    @PostMapping("/solve")
    public String checkQuiz(@RequestParam Map<String,String> allParams, Model model) {
        int[] result = quizService.solveScore(allParams);
        model.addAttribute("correct", result[0]);
        model.addAttribute("total", result[1]);
        return "result";
    }
}
