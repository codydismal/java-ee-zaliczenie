package oleksandr.fedoruk.grade.views;

import oleksandr.fedoruk.grade.models.Quiz;
import oleksandr.fedoruk.grade.service.QuizService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;


@Controller
public class QuizCreator {

    private final
    QuizService quizService;

    @Autowired
    public QuizCreator(QuizService quizService) {
        this.quizService = quizService;
    }

    @GetMapping("/new")
    public String creator(Model model) {
        return "creator";
    }

    @PostMapping("/new")
    public String createSubmit(@ModelAttribute Quiz quiz) {
        Long quizId = quizService.save(quiz);
        return "redirect:/edit?quizId=" + quizId;
    }

}