package oleksandr.fedoruk.grade.views;

import oleksandr.fedoruk.grade.models.NewQuestion;
import oleksandr.fedoruk.grade.service.QuestionService;
import oleksandr.fedoruk.grade.service.QuizService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class QuestionCreator {

    private final QuizService quizService;
    private final QuestionService questionService;

    @Autowired
    public QuestionCreator(QuizService quizService, QuestionService questionService) {
        this.quizService = quizService;
        this.questionService = questionService;
    }

    @GetMapping("/new-question")
    public String newQuestionPage(@RequestParam("quizId") long id, Model model) {
        model.addAttribute("quiz", quizService.findById(id));
        NewQuestion q = new NewQuestion();
        model.addAttribute("questionform", q);
        return "question-creator";
    }

    @PostMapping("/new-question")
    public String newQuestion(@ModelAttribute NewQuestion newQuestion) {
        questionService.saveQuestion(newQuestion);
        return "redirect:/edit?quizId=" + newQuestion.quizId;
    }
}
