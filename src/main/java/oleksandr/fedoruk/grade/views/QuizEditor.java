package oleksandr.fedoruk.grade.views;

import oleksandr.fedoruk.grade.models.Question;
import oleksandr.fedoruk.grade.models.Quiz;
import oleksandr.fedoruk.grade.service.QuestionService;
import oleksandr.fedoruk.grade.service.QuizService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class QuizEditor {

    private final QuizService quizService;
    private final QuestionService questionService;

    @Autowired
    public QuizEditor(QuizService quizService, QuestionService questionService) {
        this.quizService = quizService;
        this.questionService = questionService;
    }

    @GetMapping("/edit")
    public String creator(@RequestParam("quizId") long id, Model model) {
        Quiz quiz = quizService.findById(id);
        List<Question> questions = questionService.findByQuiz(quiz);

        model.addAttribute("quiz", quiz);
        model.addAttribute("questions", questions);

        return "questions-edit";
    }

}