package oleksandr.fedoruk.grade.views;

import oleksandr.fedoruk.grade.service.QuizService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;


@Controller
public class MainPage {

    private final
    QuizService quizService;

    @Autowired
    public MainPage(QuizService quizService) {
        this.quizService = quizService;
    }

    @GetMapping("/")
    public String listOfQuizzes(Model model) {
        model.addAttribute("quizzes", quizService.getAll());
        return "index";
    }

}