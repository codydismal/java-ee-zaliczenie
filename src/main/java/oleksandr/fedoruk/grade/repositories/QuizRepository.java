package oleksandr.fedoruk.grade.repositories;

import oleksandr.fedoruk.grade.models.Question;
import oleksandr.fedoruk.grade.models.QuizToSolve;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import oleksandr.fedoruk.grade.models.Quiz;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;


@Repository
public interface QuizRepository extends JpaRepository<Quiz, Long> {
}