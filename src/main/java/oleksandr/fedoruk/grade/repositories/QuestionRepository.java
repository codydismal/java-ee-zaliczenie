package oleksandr.fedoruk.grade.repositories;

import oleksandr.fedoruk.grade.models.Question;
import oleksandr.fedoruk.grade.models.Quiz;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface QuestionRepository extends JpaRepository<Question, Long> {
    @Query("SELECT q FROM Question q WHERE q.quiz = ?1")
    List<Question> findByQuiz(Quiz quiz);
}
