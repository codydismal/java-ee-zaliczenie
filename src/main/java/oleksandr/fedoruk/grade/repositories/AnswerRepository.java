package oleksandr.fedoruk.grade.repositories;

import oleksandr.fedoruk.grade.models.Answer;
import oleksandr.fedoruk.grade.models.Question;
import oleksandr.fedoruk.grade.models.Quiz;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;


@Repository
public interface AnswerRepository extends JpaRepository<Answer, Long> {

    @Query("SELECT a FROM Answer a where a.question = ?1")
    Collection<Answer> findAllByQuestionId(Question question);
}